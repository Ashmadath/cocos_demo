let setspr = (node, spr, h, w) => {
    if (node.node)
        node = node.node;

    node.getComponent(cc.Sprite).spriteFrame = spr;

    let aspect_h = spr._rect.height / spr._rect.width;
    let aspect_w = spr._rect.width / spr._rect.height;

    if (aspect_h < aspect_w) {
        node.height = (w || node.width) * aspect_h;
        node.width = (w || node.width);
    }
    else {
        node.width = (h || node.height) * aspect_w;
        node.height = (h || node.height)
    }
}

function _Game() {
    let self = this;

    let socket = null;

    self.Init = () => {
        self.socket = socket = io();
        // self.socket = socket = io.connect('http://localhost:5000');

        socket.on('wolf::start', self.Start);
        socket.on('wolf::win', self.Win);
        socket.on('wolf::clear', self.Clear);
        socket.on('wolf::tip', self.Tip);
        socket.on('wolf::enable', () => self.root.button_field.resumeSystemEvents(true));

        socket.on('room::joined', self.Joined);
        socket.on('room::switch', (game) => window.location = `/${game}?room=${url.searchParams.get('room')}`);
        socket.on('room::enoent', () => self.root.loading_text.string = "There is no room with this name");
        socket.on('room::full', () => self.root.loading_text.string = "This room is already full");
        let url = new URL(location.href);
        let room = url.searchParams.get('room');
        let pass = url.searchParams.get('pass');

        socket.emit('room::join', { name: room, password: pass });
    }

    self.Start = ({ role, symbols, next_symbols }) => {
        for (let i = 0; i < 9; i++) {
            setspr(self.root.icons[i], self.root.small_sprites[symbols[i]]);
        }

        self.root.role.spriteFrame = self.root.role_icons[role];

        self.symbols = symbols;
        self.next_symbols = next_symbols;
        self.role = role;
        self.root.loading.active = false;
        self.root.inactive.active = true;
        self.root.button_field.pauseSystemEvents(true);
    };

    self.Tip = (tip) => {
        self.root.inactive.active = false;
        for (let i of self.root.hints)
            i.active = false;

        let hint = self.root.hints[tip.type];
        hint.active = true;

        switch (tip.type) {
            case 0:
                hint.children[0].angle = 180 - 90 * tip.dir;
                setspr(hint.children[1], self.root.numbers[tip.count - 1]);
                setspr(hint.children[2], self.root.sprites[self.next_symbols[tip.from]], 120, 100);
                break;

            case 1:
                setspr(hint.children[0], self.root.sprites[self.next_symbols[tip.from]], 120, 100);
                hint.children[2].angle = 180 + 45 * tip.dir;
                break;

            case 2:
                setspr(hint.children[0], self.root.numbers[tip.x - 1]);
                hint.children[1].getComponent(cc.Label).string = tip.y;
                break;

            case 3:
                setspr(hint.children[0], self.root.context[tip.context]);
                break;

            case 4:
                setspr(hint.children[0], self.root.numbers[tip.count - 1]);
                setspr(hint.children[1], self.root.sprites[self.next_symbols[tip.from]], 120, 100);
                hint.children[tip.dir ? 3 : 2].active = true;
                hint.children[tip.dir ? 2 : 3].active = false;
                break;
        }
    }

    self.Send = (symbol) => socket.emit('wolf::symbol', symbol);
    self.Clear = () => {
        self.root.inactive.active = true;
        self.root.button_field.pauseSystemEvents(true);
    }

    self.Win = () => {
        self.root.win.active = true;
        setTimeout(() => {
            socket.emit('room::finished');
        }, 2000);
    }

    self.Joined = ({ count }) => self.root.loading_text.string = `Waiting for players ${count}/4`;

    return self;
}

var Game = new _Game();