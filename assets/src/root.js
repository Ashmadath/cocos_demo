
cc.Class({
    extends: cc.Component,

    properties: {
        sprites: { type: cc.SpriteFrame, default: [] },
        small_sprites: { type: cc.SpriteFrame, default: [] },
        icons: { type: cc.Sprite, default: [] },
        role_icons: { type: cc.SpriteFrame, default: [] },
        role: { type: cc.Sprite, default: null },
        inactive: { type: cc.Node, default: null },
        loading: { type: cc.Node, default: null },
        loading_text: { type: cc.Label, default: null },
        hints: { type: cc.Node, default: [] },
        numbers: { type: cc.SpriteFrame, default: [] },
        context: { type: cc.SpriteFrame, default: [] },
        win: { type: cc.Node, default: null },
        button_field: { type: cc.Node, default: null }
    },

    start() {
        Game.root = this;
        Game.Init();
    },

    send(ev, symbol) { Game.Send(symbol); }

});
