
cc.Class({
    extends: cc.Component,

    properties: {},

    start() {
        this.node.on(cc.Node.EventType.MOUSE_ENTER, () => cc.game.canvas.style.cursor = 'pointer');
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, () => cc.game.canvas.style.cursor = 'default');
    },

});
